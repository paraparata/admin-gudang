export default function ({ store, redirect }) {
  if (store.state.isLogged === true) {
    return redirect('/admin/dashboard')
  }
}
