export default function ({ store, redirect }) {
  if (store.state.isLogged === false) {
    return redirect('/')
  }
}
