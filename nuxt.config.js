import colors from 'vuetify/es5/util/colors'

export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - Sanique.88',
    title: 'Sanique.88',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/persistedState.client.js' },
    { src: '~/plugins/profile.js', ssr: false },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/firebase',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  firebase: {
    config: {
      apiKey: 'AIzaSyAeCnj8sLkvFkQM3G8mcvcB0kGFwmloOkM',
      authDomain: 'ademin-gudang.firebaseapp.com',
      databaseURL: 'https://ademin-gudang.firebaseio.com',
      projectId: 'ademin-gudang',
      storageBucket: 'ademin-gudang.appspot.com',
      messagingSenderId: '829119122485',
      appId: '1:829119122485:web:534122d0788d207c5e757c',
      measurementId: 'G-W7WM8W3953',
    },
    onFirebaseHosting: true,
    services: {
      firestore: true, // Just as example. Can be any other service.
      storage: true,
    },
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#3d5a80',
          accent: '#e0fbfc',
          secondary: '#E1F5FE',
          info: '#293241',
          warning: '#ee6c4d',
          error: '#e58387',
          success: '#b3d763',
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
