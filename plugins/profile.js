export default async ({ store, $fireStore }) => {
  const item = []
  const ref = $fireStore.collection('user')

  const data = await ref.get()
  data.forEach((doc) => {
    item.push({ foto: doc.data().foto, name: doc.data().name })
  })
  store.commit('fotoProfile', item[0].foto)
  store.commit('nameProfile', item[0].name)
}
