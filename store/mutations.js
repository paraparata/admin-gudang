export default {
  logging(state, val) {
    state.isLogged = val
  },
  fotoProfile(state, val) {
    state.foto = val
  },
  nameProfile(state, val) {
    state.name = val
  },
}
