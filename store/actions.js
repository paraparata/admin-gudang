export default {
  logging({ commit, state }) {
    setTimeout(() => {
      commit('logging', !state.isLogged)
    }, 0.5)
  },
}
